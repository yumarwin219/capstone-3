import { useState, useContext } from "react";
import { Card, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import UserContext from "../UserContext";

export default function CourseCard({ courseProp }) {
  const { user } = useContext(UserContext);
  const { name, description, price, _id } = courseProp;

  return (
    <Card className=" CourseCard p-3 mb-3 w-50">
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price</Card.Subtitle>
        <Card.Text>{price}</Card.Text>
        {user._id !== null ? (
          <>
            <Link className="btn btn-primary" to={`/courseView/${_id}`}>
              View Details
            </Link>
          </>
        ) : (
          <>
            <Link className="btn btn-primary" to={`/login`}>
              Details
            </Link>
          </>
        )}
      </Card.Body>
    </Card>
  );
}
