import { Row, Col, Card } from "react-bootstrap";
export default function Highlights() {
  return (
    <Row className="mt-3 mb-3">
      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3">
          <div id="toysImg"></div>
          <Card.Body>
            <Card.Title>
              <h2>Fishing Rods</h2>
            </Card.Title>
            <Card.Text>
              Introducing our premium fishing rod, designed to enhance your
              angling experience and reel in unforgettable catches. Crafted with
              meticulous attention to detail and utilizing cutting-edge
              materials, this fishing rod combines strength, sensitivity, and
              versatility, making it an ideal companion for anglers of all skill
              levels.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>

      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3" id="treatsImg">
          <Card.Body>
            <Card.Title>
              <h2>Fishing Reels</h2>
            </Card.Title>
            <Card.Text>
              Introducing our high-performance fishing reel, engineered to
              elevate your fishing experience to new heights. Meticulously
              designed and packed with advanced features, this reel is a
              game-changer in the world of angling. From its robust construction
              to its smooth operation, every aspect has been optimized to
              deliver unparalleled performance.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>

      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3" id="gearsImg">
          <Card.Body>
            <Card.Title>
              <h2>Jigs and Lures</h2>
            </Card.Title>
            <Card.Text>
              Introducing our collection of premium fishing lures and jigs,
              meticulously crafted to entice and outsmart even the most elusive
              fish species. Designed by avid anglers for anglers, these lures
              are the ultimate tools for enhancing your fishing success and
              reeling in trophy catches. Each lure and jig is carefully
              engineered to mimic natural prey, provoking instinctive strikes
              and maximizing your chances of landing the big one.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
