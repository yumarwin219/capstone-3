import coursesData from "../data/coursesData";
import CourseCard from "../components/CourseCard";
import { useState, useEffect } from "react";

export default function Courses() {
  const [courses, setCourses] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/activeProducts`)
      .then((res) => res.json())
      .then((data) => {
        // console.log(data);

        setCourses(
          data.map((course) => {
            return <CourseCard key={course._id} courseProp={course} />;
          })
        );
      });
  }, []);

  return (
    <>
      <h1>Available Products</h1>
      {courses}
    </>
  );
}
